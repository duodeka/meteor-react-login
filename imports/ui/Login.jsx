import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class LoginComponent extends React.Component{
    constructor(props){
        super(props);
    }

    loginHandler(e){
        const user = this.refs.username.value;
        const password = this.refs.password.value;

        Meteor.loginWithPassword(user, password, function(){

        });
    }

    render(){
        return (
            <div>
                <form>
                    <input ref="username" name="username" type="text"/> <br/>
                    <input ref="password" name="password" type="password"/><br />
                    <input
                        name="login"
                        type="button"
                        value="Log in"
                        onClick={(e) => { this.loginHandler(e)}}
                    />
                    <a href="#">Register now!</a>
                </form>
            </div>
        )
    }
}